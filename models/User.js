const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'A username must be provided!'],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'A password must be provided!']
  },
  createdDate: {
    type: Date,
    default: Date.now()
  }
});

userSchema.pre('save', async function (next) {
  this.password = await bcrypt.hash(this.password, 10);

  next();
});

const User = mongoose.model('User', userSchema);

module.exports = User;
