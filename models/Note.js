const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, 'A note must have an ID']
  },
  completed: {
    type: Boolean,
    default: false
  },
  text: {
    type: String,
    required: [true, 'A note must have some text']
  },
  createdDate: {
    type: Date,
    default: Date.now()
  }
}, { timestamps: true });

const Note = mongoose.model('Note', noteSchema)

module.exports = Note;
