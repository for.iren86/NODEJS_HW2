const express = require('express');
const router = express.Router();
const { getUser, deleteUser, updatePassword } = require('../controllers/userController');
const authMiddleware = require('../middleware/authMiddleware');

router.use(authMiddleware);

router.route('/').get(getUser).delete(deleteUser).patch(updatePassword);

module.exports = router;
