const User = require('../models/User');
const AppError = require('../errors/appError');
const bcrypt = require('bcryptjs');
const catchAsync = require('../middleware/catch');

const getUser = catchAsync(async (req, res, next) => {
  const user = await User.findById(req.user._id);

  if (!user) {
    return next(new AppError('Unauthorized', 401));
  }

  const { _id, username, createdDate } = user;

  res.status(200).json({ user: { _id, username, createdDate } });
});

const deleteUser = catchAsync(async (req, res, next) => {
  await User.findOneAndRemove(req.user._id);
  res.status(200).json({ message: 'Success' })
});

const updatePassword = catchAsync(async (req, res, next) => {
  const user = await User.findById(req.user._id);
  const { _id, password } = user;

  const { oldPassword, newPassword } = req.body;
  const isPasswordCorrect = await bcrypt.compare(oldPassword, password);

  if (!isPasswordCorrect) {
    return next(new AppError('Your current password is wrong', 400));
  }

  const hashedPassword = await bcrypt.hash(newPassword, 10);

  await User.findByIdAndUpdate(_id, { password: hashedPassword });

  res.status(200).json({ message: 'Success' });

});

module.exports = { getUser, deleteUser, updatePassword };
