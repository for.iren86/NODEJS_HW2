const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const AppError = require('../errors/appError');
const catchAsync = require('../middleware/catch');


const registerUser = catchAsync(async (req, res, next) => {
  const credentials = req.body;
  const { username, password } = credentials;

  if (!Object.keys(credentials).length) {
    return next(new AppError('Credentials must be provided!', 400));
  }

  if (!username || !password) {
    return next(new AppError('Username and password must be provided!', 400));
  }

  const newUser = await User.create(credentials);

  res.status(200).json({ message: 'Success' });
});

const loginUser = catchAsync(async (req, res, next) => {
  const credentials = req.body;
  const { username, password } = credentials;

  if (!Object.keys(credentials).length) {
    return next(new AppError('Credentials must be provided!', 400));
  }


  if (!username || !password) {
    return next(new AppError('Username and password must be provided!', 400));
  }

  const user = await User.findOne({ username });

  if (!user || !(await bcrypt.compare(password, user.password))) {
    return next(new AppError('Wrong username or password!', 400));
  }

  const jwt_token = jwt.sign({
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
        password: user.password,
      }, process.env.JWT_SECRET,
      { expiresIn: '90h' });

  res.status(200).json({
    message: 'Success', jwt_token
  });
});

module.exports = { registerUser, loginUser };
