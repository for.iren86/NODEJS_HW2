const Note = require('../models/Note');
const AppError = require('../errors/appError');
const catchAsync = require('../middleware/catch');

const getNotes = catchAsync(async (req, res, next) => {
  const { offset = 0, limit = 0 } = req.query;

  const notes = await Note.find({ userId: req.user._id }).skip(offset).limit(limit);

  res.status(200).json({ offset, limit, count: notes.length, notes });
});


const createNote = catchAsync(async (req, res, next) => {
  const { text } = req.body;

  if (!text) {
    return next(new AppError('Text for note is not provided', 400));
  }
  const newNote = await Note.create({
    text,
    userId: req.user._id
  });

  res.status(200).json({
    message: "Success"
  });

});

const getNote = catchAsync(async (req, res, next) => {
  const _id = req.params.id;

  if (!_id) {
    return next(new AppError('Id not provided', 400));
  }
  const note = await Note.findById(_id);

  if (!note) {
    return next(new AppError('There is no note with this id', 400));
  }
  if (note.userId !== req.user._id) {
    return next(new AppError('Forbidden'), 403);
  }
  res.status(200).json({
    note
  });
});

const editNote = catchAsync(async (req, res, next) => {
  const _id = req.params.id;
  const { text } = req.body;

  if (!_id) {
    return next(new AppError('Id not provided', 400));
  }
  const note = await Note.findById(_id);

  if (!note) {
    return next(new AppError('There is no note with this id', 400));
  }
  if (note.userId !== req.user._id) {
    return next(new AppError('Forbidden'), 403);
  }

  const newNote = await Note.findByIdAndUpdate(_id, { text }, { new: true });

  res.status(200).json({
    message: 'success',
    text: newNote.text
  });
});

const updateNote = catchAsync(async (req, res, next) => {
  const _id = req.params.id;

  if (!_id) {
    return next(new AppError('Id not provided', 400));
  }
  const note = await Note.findById(_id);

  if (!note) {
    return next(new AppError('There is no note with this id', 400));
  }
  if (note.userId !== req.user._id) {
    return next(new AppError('Forbidden'), 403);
  }

  const updatedNote = await Note.findByIdAndUpdate(_id, { completed: !note.completed }, { new: true });

  res.status(200).json({
    message: 'success',
    completed: updatedNote.completed
  });

});

const deleteNote = catchAsync(async (req, res, next) => {
  const _id = req.params.id;

  if (!_id) {
    return next(new AppError('Id not provided', 400));
  }
  const note = await Note.findById(_id);

  if (!note) {
    return next(new AppError('There is no note with this id', 400));
  }
  if (note.userId !== req.user._id) {
    return next(new AppError('Forbidden'), 403);
  }

  await Note.findByIdAndDelete(_id);

  res.status(200).json({ message: "Success" });
});

module.exports = { getNotes, createNote, getNote, editNote, updateNote, deleteNote };
