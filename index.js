require('dotenv').config({ path: './config.env' });
const express = require('express');
const morgan = require('morgan');
const app = express();
const path = require('path');
const fs = require('fs');
const connectDB = require('./database/connect');
const AppError = require('./errors/appError');
const globalErrorHandler = require('./middleware/errorMiddleware');
const port = process.env.PORT || 8080;

const DB = process.env.DB_URI.replace('<PASSWORD>', process.env.DATABASE_PASSWORD);

app.use(
    express.urlencoded({
      extended: true,
    })
);
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', require('./routers/authRouter'));
app.use('/api/notes', require('./routers/notesRouter'));
app.use('/api/users/me', require('./routers/userRouter'));

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });
app.use(morgan('combined', { stream: accessLogStream }));


const start = async () => {
  try {
    await connectDB(DB);
    app.listen(port, () => {
      console.log(`App running on port ${port}`);
    });
  }
  catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

app.use(globalErrorHandler);
